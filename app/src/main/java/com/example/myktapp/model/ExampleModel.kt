package com.example.myktapp.model

import android.widget.TextView
import android.R
import android.view.View
import androidx.annotation.ColorInt
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.example.myktapp.CarouselData
import com.example.myktapp.SampleController
import kotlinx.android.synthetic.main.view_header.view.*

@EpoxyModelClass(layout = com.example.myktapp.R.layout.view_header)
class ExampleModel : EpoxyModelWithHolder<ExampleModel.Holder>() {

    fun ExampleModel(carousel: CarouselData, callbacks: SampleController.AdapterCallbacks): ??? {
        super(com.example.myktapp.R.layout.model_carousel_group, buildModels(carousel, callbacks))
        this.data = carousel
        id(carousel.id)
    }

    @EpoxyAttribute
    var text: String = "test"

    override fun bind(holder: Holder) {
        holder.header.title_text.text = text
    }

    override fun bind(holder: Holder, previouslyBoundModel: EpoxyModel<*>) {

    }

    override fun unbind(holder: Holder) {
    }

    class Holder : BaseEpoxyHolder() {
        val header: View by bind(com.example.myktapp.R.id.title_text)
    }
}