package com.example.myktapp.model;

import com.airbnb.epoxy.EpoxyModel;
import com.airbnb.epoxy.EpoxyModelGroup;
import com.example.myktapp.CarouselData;
import com.example.myktapp.ColorData;
import com.example.myktapp.R;
import com.example.myktapp.SampleController;
import com.example.myktapp.view.GridCarouselModel_;

import java.util.ArrayList;
import java.util.List;

public class CarouselModelGroup extends EpoxyModelGroup {
    public final CarouselData data;

    public CarouselModelGroup(CarouselData carousel, SampleController.AdapterCallbacks callbacks) {
        super(R.layout.model_carousel_group, buildModels(carousel, callbacks));
        this.data = carousel;
        id(carousel.getId());
    }

    private static List<EpoxyModel<?>> buildModels(CarouselData carousel,
                                                   SampleController.AdapterCallbacks callbacks) {
        List<ColorData> colors = carousel.getColors();
        ArrayList<EpoxyModel<?>> models = new ArrayList<>();

        List<ColorModel_> colorModels = new ArrayList<>();
        for (ColorData colorData : colors) {
            colorModels.add(new ColorModel_()
                    .id(colorData.getId(), carousel.getId())
                    .color(colorData.getColorInt())
                    .playAnimation(colorData.shouldPlayAnimation()));
        }

        models.add(new GridCarouselModel_()
                .id("carousel")
                .models(colorModels));

        return models;
    }

    @Override
    public int getSpanSize(int totalSpanCount, int position, int itemCount) {
        return totalSpanCount;
    }
}