package com.example.myktapp.model

import android.animation.Animator
import android.system.Os.bind
import android.view.View
import androidx.annotation.ColorInt
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.myktapp.R

@EpoxyModelClass(layout = R.layout.model_color)
abstract class ColorModel : EpoxyModelWithHolder<ColorModel.ColorHolder>() {
    @EpoxyAttribute
    @ColorInt
    var color: Int = 0
    @EpoxyAttribute
    var playAnimation: Boolean = false
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash) var clickListener: View.OnClickListener? = null

    override fun bind(holder: ColorHolder) {
        holder.cardView.setBackgroundColor(color)
        holder.cardView.setOnClickListener(clickListener)
    }

    override fun bind(holder: ColorHolder, previouslyBoundModel: EpoxyModel<*>) {

    }

    override fun unbind(holder: ColorHolder) {
        // Don't leak the click listener when this view goes back in the view pool
        holder.cardView.setOnClickListener(null)
    }

    class ColorHolder : BaseEpoxyHolder() {
        val cardView: View by bind(R.id.card_view)
    }
}