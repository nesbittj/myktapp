package com.example.myktapp

import android.graphics.Color
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.airbnb.epoxy.EpoxyRecyclerView
import java.util.*
import kotlin.collections.ArrayList

class BottomNavFirstFragment : Fragment(), SampleController.AdapterCallbacks {

    override fun onColorClicked(carousel: CarouselData?, colorPosition: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val CAROUSEL_DATA_KEY = "carousel_data_key"

    private val controller = SampleController(this)
    private var carousels: MutableList<ExampleData> = ArrayList<ExampleData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        recycler_view?.layoutManager = GridLayoutManager(activity, 2)
//        recycler_view?.setController(controller)

        if (savedInstanceState != null) {
            carousels = savedInstanceState.getParcelableArrayList<ExampleData>(CAROUSEL_DATA_KEY)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_bottom_nav_first, container, false)

        val recyclerView = v?.findViewById(R.id.recycler_view) as EpoxyRecyclerView
        recyclerView.layoutManager = GridLayoutManager(activity, 2)
        recyclerView.setController(controller)

        carousels.add(0, ExampleData("test"))

        updateController()

        return v
    }

    override fun onSaveInstanceState(state: Bundle) {
        super.onSaveInstanceState(state)
        state.putParcelableArrayList(CAROUSEL_DATA_KEY, carousels as ArrayList<out Parcelable>)
        controller.onSaveInstanceState(state)
    }

    private fun updateController() {
        controller.setData(carousels)
    }
}
