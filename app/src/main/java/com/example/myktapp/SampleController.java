package com.example.myktapp;

import com.airbnb.epoxy.AutoModel;
import com.airbnb.epoxy.TypedEpoxyController;
import com.example.myktapp.model.CarouselModelGroup;
import com.example.myktapp.model.ExampleModel;

import java.util.List;

import static com.airbnb.epoxy.EpoxyAsyncUtil.getAsyncBackgroundHandler;

public class SampleController extends TypedEpoxyController<List<ExampleData>> {
    public interface AdapterCallbacks {
        void onColorClicked(CarouselData carousel, int colorPosition);
    }

//    @AutoModel
//    HeaderViewModel_ header;

    private final AdapterCallbacks callbacks;

    SampleController(AdapterCallbacks callbacks) {
        // Demonstrating how model building and diffing can be done in the background.
        // You can control them separately by passing in separate handler, as shown below.
        super(getAsyncBackgroundHandler(), getAsyncBackgroundHandler());
//    super(new Handler(), BACKGROUND_HANDLER);
//    super(BACKGROUND_HANDLER, new Handler());

        this.callbacks = callbacks;
        setDebugLoggingEnabled(true);
    }

    @Override
    protected void buildModels(List<ExampleData> carousels) {
//        header
//                .title("Epoxy")
//                .caption("Subtitle");
        // "addTo" is not needed since implicit adding is enabled
        // (https://github.com/airbnb/epoxy/wiki/Epoxy-Controller#implicit-adding)

        for (int i = 0; i < carousels.size(); i++) {
            ExampleData carousel = carousels.get(i);
            add(new ExampleModel());
        }
    }

    @Override
    protected void onExceptionSwallowed(RuntimeException exception) {
        // Best practice is to throw in debug so you are aware of any issues that Epoxy notices.
        // Otherwise Epoxy does its best to swallow these exceptions and continue gracefully
        throw exception;
    }
}
