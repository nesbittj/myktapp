package com.example.myktapp.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.airbnb.epoxy.ModelProp;
import com.airbnb.epoxy.ModelView;
import com.airbnb.epoxy.TextProp;
import com.example.myktapp.R;

//@ModelView
//public class HeaderView extends LinearLayout {
//
//    private TextView title;
//    private TextView caption;
//
//    public HeaderView(Context context) {
//        super(context);
//        init();
//    }
//
//    private void init() {
//        setOrientation(VERTICAL);
//        inflate(getContext(), R.layout.view_header, this);
//        title = findViewById(R.id.title_text);
//        caption = findViewById(R.id.caption_text);
//    }
//
//    @TextProp(defaultRes = R.string.app_name)
//    public void setTitle(CharSequence title) {
//        this.title.setText(title);
//    }
//
//    @ModelProp(options = ModelProp.Option.GenerateStringOverloads)
//    public void setCaption(CharSequence caption) {
//        this.caption.setText(caption);
//    }
//}
