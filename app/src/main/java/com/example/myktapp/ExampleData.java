package com.example.myktapp;

import android.os.Parcel;
import android.os.Parcelable;

public class ExampleData implements Parcelable {

    private final String text;

    public ExampleData(String text) {
        this.text = text;
    }

    protected ExampleData(Parcel in) {
        text = in.readString();
    }

    public String getText() {
        return text;
    }

    public static final Creator<ExampleData> CREATOR = new Creator<ExampleData>() {
        @Override
        public ExampleData createFromParcel(Parcel in) {
            return new ExampleData(in);
        }

        @Override
        public ExampleData[] newArray(int size) {
            return new ExampleData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text);
    }
}
